<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        $fighters = $arena->all();
        $result = 'Арена пуста, додайте бійців';
        if(!empty($fighters)) {
            $result = '';
            foreach ($arena->all() as $fighter) {
                $result .= '<img src="' . $fighter->getImage() . '">' . $fighter->getName() . ': ' . $fighter->getAttack() . ', ' . $fighter->getHealth() . "<br>\n";
            }
        }
        return $result;
    }
}
