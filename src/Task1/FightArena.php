<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    private $fighters = [];

    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        $result = [
            'res_index' => 0,
            'max_attack' => $this->fighters[0]->getAttack() ?: 0,
        ];
        foreach ($this->fighters as $key => $fighter){
            if($fighter->getAttack() > $result['max_attack']){
                $result['res_index'] = $key;
                $result['max_attack'] = $fighter->getAttack();
            }
        }
        return $this->fighters[$result['res_index']];
    }

    public function mostHealthy(): Fighter
    {
        $result = [
            'res_index' => 0,
            'max_health' => $this->fighters[0]->getHealth() ?: 0,
        ];
        foreach ($this->fighters as $key => $fighter){
            if($fighter->getHealth() > $result['max_health']){
                $result['res_index'] = $key;
                $result['max_health'] = $fighter->getHealth();
            }
        }
        return $this->fighters[$result['res_index']];
    }

    public function all(): array
    {
        return $this->fighters;
    }
}
